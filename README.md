# ros_openvino_tiny-yolov3

This repository contains ROS Service for Intel Openvino based yolov3 object detection.

Intel OpenVino 2018 R5 suported

##Converting darknet model to openvino

Use **https://github.com/PINTO0309/OpenVINO-YoloV3/releases/tag/2018R5** release.
Only cpp demo tested

MOST IMPORTANT:
Use only with tensorflow-1.12.0. Other versions of tensorflow won't work.
