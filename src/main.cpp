// Copyright (C) 2018 Intel Corporation
// SPDX-License-Identifier: Apache-2.0
//

/**
 * \brief The entry point for the Inference Engine object_detection demo
 * application \file object_detection_demo_yolov3_async/main.cpp \example
 * object_detection_demo_yolov3_async/main.cpp
 */

#include "object_detection_demo_yolov3_async.hpp"

InferRequest::Ptr async_infer_request_curr;
std::string inputName;
CNNNetReader netReader;
OutputsDataMap outputInf;
bool auto_resize;
InputsDataMap inputInf;
int width;
int height;
std::vector<std::string> labels;
typedef std::chrono::duration<double, std::ratio<1, 1000>> ms;

bool getObjects(openvino_object_detection::Objects::Request &req,
                openvino_object_detection::Objects::Response &res) {
  auto t = std::chrono::high_resolution_clock::now();
  cv::Mat frame = cv_bridge::toCvCopy(req.img, "bgr8")->image;

  FrameToBlob(frame, async_infer_request_curr, inputName, auto_resize);

  async_infer_request_curr->StartAsync();

  if (OK ==
      async_infer_request_curr->Wait(IInferRequest::WaitMode::RESULT_READY)) {

    // ---------------------------Processing output
    // blobs-------------------------------------------------- Processing
    // results of the CURRENT request
    unsigned long resized_im_h = inputInf.begin()->second.get()->getDims()[0];
    unsigned long resized_im_w = inputInf.begin()->second.get()->getDims()[1];
    std::vector<DetectionObject> objects;
    // Parsing outputs
    for (auto &output : outputInf) {
      auto output_name = output.first;
      // slog::info << "output_name = " + output_name << slog::endl;

      CNNLayerPtr layer =
          netReader.getNetwork().getLayerByName(output_name.c_str());
      Blob::Ptr blob = async_infer_request_curr->GetBlob(output_name);
      ParseYOLOV3Output(layer, blob, resized_im_h, resized_im_w, height, width,
                        req.t, objects);
    }
    // Filtering overlapping boxes
    std::sort(objects.begin(), objects.end());
    for (int i = 0; i < objects.size(); ++i) {
      if (objects[i].confidence == 0)
        continue;
      for (int j = i + 1; j < objects.size(); ++j) {
        if (IntersectionOverUnion(objects[i], objects[j]) >= req.iou_t) {
          objects[j].confidence = 0;
        }
        // if (objects[j].confidence == 1) {
        //    objects[j].confidence = 0;
        //}
      }
    }
    res.len = 0;
    for (auto &object : objects) {
      if (object.confidence < req.t)
        continue;
      openvino_object_detection::Object tmp;
      tmp.confidence = object.confidence;
      tmp.label = labels[object.class_id];
      tmp.id = object.class_id;
      tmp.x = (object.xmin + object.xmax) / 2.0;
      tmp.y = (object.ymin + object.ymax) / 2.0;
      tmp.w = object.xmax - object.xmin;
      tmp.h = object.ymax - object.ymin;
      res.objects.push_back(tmp);
      res.len++;
    }
  }
  res.ms = std::chrono::duration_cast<ms>(
               std::chrono::high_resolution_clock::now() - t)
               .count();
  return true;
}

int main(int argc, char *argv[]) {

  ros::init(argc, argv, "object_detection");
  ros::NodeHandle n;
  ros::ServiceServer conf_service =
      n.advertiseService("tiny_yolov3", getObjects);
  std::string dev;
  std::string def = "CPU";
  n.param("/object_detection/target", dev, def);
  n.param("/object_detection/width", width, 648);
  n.param("/object_detection/height", height, 488);
  auto_resize = n.hasParam("/object_detection/auto_resize");
  std::cout << dev.c_str() << std::endl;
  try {
    /** This demo covers a certain topology and cannot be generalized for any
     * object detection **/
    std::cout << "InferenceEngine: " << GetInferenceEngineVersion()
              << std::endl;

    // -----------------------------------------------------------------------------------------------------

    // --------------------------- 1. Load Plugin for inference engine
    // -------------------------------------
    slog::info << "Loading plugin" << slog::endl;
    InferencePlugin plugin =
        PluginDispatcher({"../lib", ""}).getPluginByDevice(dev.c_str());
    printPluginVersion(plugin, std::cout);

    /**Loading extensions to the plugin **/

    /** Loading default extensions **/
    if (dev == "CPU") {
      /**
       * cpu_extensions library is compiled from the "extension" folder
       *containing custom CPU layer implementations.
       **/
      plugin.AddExtension(std::make_shared<Extensions::Cpu::CpuExtensions>());
    }

    if (n.hasParam("/object_detection/l")) {
      std::string l_flags;
      n.getParam("/object_detection/l", l_flags);
      // CPU extensions are loaded as a shared library and passed as a pointer
      // to the base extension
      IExtensionPtr extension_ptr =
          make_so_pointer<IExtension>(l_flags.c_str());
      plugin.AddExtension(extension_ptr);
    }
    if (n.hasParam("/object_detection/c")) {
      std::string c_flags;
      n.getParam("/object_detection/c", c_flags);
      // GPU extensions are loaded from an .xml description and OpenCL kernel
      // files
      plugin.SetConfig(
          {{PluginConfigParams::KEY_CONFIG_FILE, c_flags.c_str()}});
    }

    /** Per-layer metrics **/
    if (n.hasParam("/object_detection/pc")) {
      plugin.SetConfig(
          {{PluginConfigParams::KEY_PERF_COUNT, PluginConfigParams::YES}});
    }
    // -----------------------------------------------------------------------------------------------------

    // --------------- 2. Reading the IR generated by the Model Optimizer
    // (.xml and .bin files) ------------
    slog::info << "Loading network files" << slog::endl;
    /** Reading network model **/
    std::string model;
    if (!n.getParam("/object_detection/model", model)) {
      std::cout << "Model not found";
      return 0;
    }
    netReader.ReadNetwork(model);
    /** Setting batch size to 1 **/
    slog::info << "Batch size is forced to  1." << slog::endl;
    netReader.getNetwork().setBatchSize(1);
    /** Extracting the model name and loading its weights **/
    std::string binFileName = fileNameNoExt(model) + ".bin";
    netReader.ReadWeights(binFileName);
    /** Reading labels (if specified) **/
    std::string labelFileName = fileNameNoExt(model) + ".labels";
    std::vector<std::string> label_list;
    std::ifstream inputFile(labelFileName);
    std::copy(std::istream_iterator<std::string>(inputFile),
              std::istream_iterator<std::string>(),
              std::back_inserter(label_list));
    labels = label_list;
    // -----------------------------------------------------------------------------------------------------

    /** YOLOV3-based network should have one input and three output **/
    // --------------------------- 3. Configuring input and output
    // -----------------------------------------
    // --------------------------------- Preparing input blobs
    // ---------------------------------------------
    slog::info << "Checking that the inputs are as the demo expects"
               << slog::endl;
    InputsDataMap inputInfo(netReader.getNetwork().getInputsInfo());
    inputInf = inputInfo;
    if (inputInfo.size() != 1) {
      throw std::logic_error(
          "This demo accepts networks that have only one input");
    }
    InputInfo::Ptr &input = inputInfo.begin()->second;
    inputName = inputInfo.begin()->first;
    input->setPrecision(Precision::U8);
    if (n.hasParam("auto_resize")) {
      input->getPreProcess().setResizeAlgorithm(
          ResizeAlgorithm::RESIZE_BILINEAR);
      input->getInputData()->setLayout(Layout::NHWC);
    } else {
      input->getInputData()->setLayout(Layout::NCHW);
    }
    // --------------------------------- Preparing output blobs
    // -------------------------------------------
    slog::info << "Checking that the outputs are as the demo expects"
               << slog::endl;
    OutputsDataMap outputInfo(netReader.getNetwork().getOutputsInfo());
    outputInf = outputInfo;
    // if (outputInfo.size() != 3) {
    //    throw std::logic_error("This demo only accepts networks with three
    //    layers");
    //}
    for (auto &output : outputInfo) {
      output.second->setPrecision(Precision::FP32);
      output.second->setLayout(Layout::NCHW);
    }
    // -----------------------------------------------------------------------------------------------------

    // --------------------------- 4. Loading model to the plugin
    // ------------------------------------------
    slog::info << "Loading model to the plugin" << slog::endl;
    ExecutableNetwork network = plugin.LoadNetwork(netReader.getNetwork(), {});

    // -----------------------------------------------------------------------------------------------------

    // --------------------------- 5. Creating infer request
    // -----------------------------------------------

    async_infer_request_curr = network.CreateInferRequestPtr();
    // -----------------------------------------------------------------------------------------------------

    // --------------------------- 6. Doing inference
    // ------------------------------------------------------
    slog::info << "Start inference " << slog::endl;

    ros::spin();

    /** Showing performace results **/
    if (n.hasParam("/object_detection/pc")) {
      printPerformanceCounts(*async_infer_request_curr, std::cout);
    }
  } catch (const std::exception &error) {
    std::cerr << "[ ERROR ] " << error.what() << std::endl;
    return 1;
  } catch (...) {
    std::cerr << "[ ERROR ] Unknown/internal exception happened." << std::endl;
    return 1;
  }

  slog::info << "Execution successful" << slog::endl;
  return 0;
}
