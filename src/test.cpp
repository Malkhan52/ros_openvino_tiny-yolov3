#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include <openvino_object_detection/Object.h>
#include <openvino_object_detection/Objects.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include<string>
using namespace cv;

int main(int argc, char **argv) {
  ros::init(argc, argv, "object_detection_test");
  // Handle creation
  ros::NodeHandle n;
  Mat frame;
  int count = 0;
  VideoCapture cap(argv[1]);
  ros::ServiceClient client =
      n.serviceClient<openvino_object_detection::Objects>("/tiny_yolov3");
  openvino_object_detection::Objects t;
  sensor_msgs::Image output_image_msg;
  cv::namedWindow("view");

  while (1) {
    cap >> frame;

    t.request.t = 0.1;
    t.request.iou_t = 0.1;
    t.request.img =
        *cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame).toImageMsg();
    if (client.call(t)) {
      // std::cout << t.response.ms << "\n";

      for (openvino_object_detection::Object obj : t.response.objects) {
        std::ostringstream conf;
        conf << ":" << std::fixed << std::setprecision(3) << obj.confidence;
        cv::putText(frame, (std::string)obj.label + conf.str(),
                    cv::Point2f(obj.x - obj.w / 2, obj.y - obj.h / 2 - 5),
                    cv::FONT_HERSHEY_COMPLEX_SMALL, 1, cv::Scalar(0, 0, 255), 1,
                    cv::LINE_AA);
        cv::rectangle(frame, cv::Point2f(obj.x - obj.w / 2, obj.y - obj.h / 2),
                      cv::Point2f(obj.x + obj.w / 2, obj.y + obj.h / 2),
                      cv::Scalar(0, 0, 255), 1, cv::LINE_AA);
        cv::circle(frame, cv::Point2f(obj.x,obj.y),2,Scalar(0,0,0),-1);
        cv::circle(frame, cv::Point2f(324, 244),2,Scalar(0,0,0),-1);
        cv::line(frame, cv::Point2f(obj.x,obj.y),cv::Point2f(324,244),Scalar(0,0,0),1);
          std::cout<<(std::string)obj.label + conf.str()<<"\t"<<"Width: "<<((obj.x + obj.w / 2)-(obj.x - obj.w / 2))<<"\tHeight: "<<((obj.y + obj.h / 2)-(obj.y - obj.h / 2))<<"\n";
        if ((std::string)obj.label=="Jiangshi")
      {
        std::cout<<"Height with W: "<<((obj.y - 244)*0.40)<<"\n";
        std::cout<<"Dist with W: "<<(58*375.426666)/(((obj.x + obj.w / 2)-(obj.x - obj.w / 2))*0.74)<<"\n";
        std::cout<<"Dist with H: "<<(93*377.57088519)/(((obj.y + obj.h / 2)-(obj.y - obj.h / 2))*0.74)<<"\n";
      }
        //std::cout<<(std::string)obj.label<<"\t"<<((obj.x + obj.w / 2)-(obj.x - obj.w / 2))*((obj.y + obj.h / 2)-(obj.y - obj.h / 2))<<"\n";

        // std::cout<<(std::string)obj.label<<"\t"<<"y: "<<324-obj.x<<"\t"<<"z: "<<244-obj.y<<"\n";
        
        // std::cout<<(std::string)obj.label<<"\t"<<"y(in mm): "<<(324-obj.x)*7.4<<"\t"<<"z(in mm): "<<(244-obj.y)*7.4<<"\n";
        // cv::imwrite(std::to_string(count)+".png",frame);
        // if (count>10)
        // {
        //   exit(0);
        // }
        // count++;
      }
      cv::imshow("view", frame);
      cv::waitKey(1);
    }
    ros::spinOnce();
  }
}
